# Hello, Promobit!



## Getting started

!Ssh must be installed on the server.

Install components on the client: 

* python3
* ansible
* sshpass

```
sudo apt-get update
sudo apt install python3
sudo apt install ansible
sudo apt install sshpass
```

Setings sshpass:

```
ssh-keygen -t rsa
ssh-copy-id user@server
```

Setings ansible:

```
sudo mkdir /etc/ansible/
```
Create a file 'hosts' along the path '/etc/ansible/'.
Enter the name of the group in the file and add the hosts there.
Example:

```
[srv]
srv-web

```

Download the playbook:

```
cd /etc/ansible/
sudo wget https://gitlab.com/myproject8800122/promobit/-/raw/main/playbook-nginx-promobit.yml
```

Before launching the playbook, specify your required hosts group.

Launching the ansible playbook:

```
ansible-playbook /etc/ansible/playbook-nginx-promobit.yml -kK
```

## Сheck

It remains to check the result by logging into the web-server.
